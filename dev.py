import psutil
import signal
import os
import time
PID_FILE = 'process.pid'

def get_pid():
    """Returns last recorded pid file"""
    if os.path.exists(PID_FILE) and os.path.isfile(PID_FILE):
        with open(PID_FILE, 'r') as f:
            pid = f.readline()
        pid = int(pid)
        return pid
    else:
        return None

pid = get_pid()
# os.kill(pid, signal.SIGTERM)   # send Ctrl-C signal
# time.sleep(1)
# print(psutil.pid_exists(pid))
# import json
# p = psutil.Process(pid)
# print(json.dumps(p.as_dict(), indent=4, sort_keys=True))

print("Process terminated")