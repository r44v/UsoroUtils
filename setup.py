from setuptools import setup

# pip install --editable .

def get_packages_from_pipenv():
    """Gets packages from pipfile"""
    import configparser
    config = configparser.ConfigParser()
    config.read('Pipfile')
    return list(config['packages'])

setup(
    name='usoro',
    description='Usoro Utilities module',
    version='0.0.5',
    packages=['usoro'],
    install_requires=get_packages_from_pipenv(),
)