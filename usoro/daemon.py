"""
Create a daemon
from usoro.daemon import cli, linux_callback
linux_callback.put(callback_function)
cli() # starts click commandline interface
"""

import click
import os
import platform
import psutil
import signal
import sys
import time

PID = -1 # should always be changed with get_pid()

def default_func():
    print("No default option set")

options_dict = {
    "linux_callback": None,
    "executable": os.path.join(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')), 'app.py'),
    "default_func": default_func,
    "pid_file": None
}

def start_process():
    """
    Starts daemon process
        imports are after platform selection
    """

    if platform.system() == 'Windows':
        import subprocess       

        executable = [sys.executable, options_dict.get('executable')]

        p = subprocess.Popen(executable, shell=False, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        if p.stdin is not None:
            p.stdin.close() #Prevent input (should not actually happen but just in case)
    else:
        import daemon as d
        if options_dict.get("linux_callback") is not None:
            with d.DaemonContext():
                options_dict.get("linux_callback")()
        else:
            print('Missing callback')

def get_pid_file():
    pid_file = options_dict.get('pid_file')
    if pid_file is None:
        pid_file = os.path.join(os.path.dirname(options_dict.get('executable')), 'process.pid')
    return pid_file

def get_pid():
    """Returns last recorded pid file"""

    pid_file = get_pid_file()
    if os.path.exists(pid_file) and os.path.isfile(pid_file):
        with open(pid_file, 'r') as f:
            pid = f.readline()
        pid = int(pid)
        return pid
    else:
        return None

def is_running():
    """Returns True if pid is running"""
    if PID is not None:
        return psutil.pid_exists(PID)
    else:
        return False


def kill():
    """Kills running process"""

    pid_file = get_pid_file()

    process = psutil.Process(PID)
    process.terminate() # SIGTERM
    try:
        process.wait(timeout=5)
    except psutil.TimeoutExpired:
        print('SIGTERM failed, now using SIGKILL')
        process.kill() # SIGKILL
    time.sleep(1)
    if is_running():
        return False
    else:
        if os.path.exists(pid_file):
            os.remove(pid_file)
        return True


def start():
    """Starts process in background"""
    if is_running():
        print("Process already running")
    else:
        print("Starting process")
        start_process()
        try:
            pass
        except:
            print('ERROR starting process')

        time.sleep(1) # give process time to start and update pidfile

        global PID
        PID = get_pid()
        if not is_running():
            print("Starting process failed")


def stop():
    """Kills process running in background"""
    if not is_running():
        print("Process not running")
    else:
        print("Killing process")
        if not kill():
            print("Killing process failed (PID = %d)" % PID)


def status():
    """Show pid of running process or not running message"""
    print('Platform:', platform.system())
    if is_running():
        print("Process running (PID = %d)" % PID) 
    else:
        print("Process not running")


@click.command('start')
def command_start():
    """Starts process in background"""
    start()

@click.command('stop')
def command_stop():
    """Kills process running in background"""
    stop()

@click.command('restart')
def command_restart():
    """First calls stop and afterwords calls start"""
    stop()
    start()

@click.command('status')
@click.option('--code', is_flag=True, help='return code 0 if process runs otherwist 1 prints nothing')
@click.option('--pid', is_flag=True, help='Returns pid or -1 if process is not running')
def command_status(code, pid):
    """Show pid of running process or not running message"""
    if pid:
        print(PID if is_running() else -1)
    if code:
        sys.exit(0 if is_running() else 1)
    if not (pid or code):
        status()


@click.command('run')
def command_run():
    """Start application not in daemon mode"""
    options_dict.get("default_func")()

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])
@click.group(context_settings=CONTEXT_SETTINGS)
def cli():
    global PID
    PID = get_pid()
    pass

cli.add_command(command_run)
cli.add_command(command_start)
cli.add_command(command_stop)
cli.add_command(command_restart)
cli.add_command(command_status)