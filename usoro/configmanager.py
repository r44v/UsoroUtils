import yaml
import os
import sys
from usoro.logger import Log

TAG = os.path.basename(__file__)

class ConfigError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return self.value


class ConfigManager():
    def __init__(self, config_file = 'config.yml'):
        self.__config_file = config_file
        self.__config = None # don't do anything until needed (lazy loading)

    def __get_config(self):
        """Getter for __config
        
        Returns:
            [dict] -- Full config dict
        """

        if self.__config is None:
            self.initialize()
        return self.__config
        
    def __set_config(self, data):
        self.__config = data

    def initialize(self):
        Log.Debug(TAG, "Loading: %s" % self.__config_file)

        # Create example config file based on template
        if not (os.path.exists(self.example_config_name())
                and os.path.isfile(self.example_config_name())):
            with open(self.example_config_name(), 'w') as f:
                yaml.dump(self.__template(), f, default_flow_style=False)

        # Check is config_file exists
        if not (os.path.exists(self.__config_file) and os.path.isfile(self.__config_file)):
            raise ConfigError('%s file not found' % self.__config_file
                +' Please use example_config.yml to create a correct config')

        # Load config data
        with open(self.__config_file, 'r') as f:
            self.__set_config(yaml.load(f))
    
    def get(self, dot_string, default=None):
        """Get config item
        
        Arguments:
            dot_string {string} -- "catergory.catergory.item"
                
        Keyword Arguments:
            default {config item} -- config item default value (default: {None})
        
        Returns:
            [config item] -- [config value or default]
        """


        d = self.__get_config() # reference copy
        keys = str.split(dot_string, '.')
        for key in keys:
            d = d.get(key, None)
            if d is None:
                d = default
                break

        return d
    
    @staticmethod
    def example_config_name():
        return 'example_config.yml'

    def __template(self):
        """Config file template
        
        Returns:
            dict -- Example config
        """

        config_template = {
            "version": "0.0.1",
        }

        return config_template

# make config importable    
config = ConfigManager()