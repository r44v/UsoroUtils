import os
import inspect
import signal
import datetime
import sys

class GracefulKiller:
    """
    Can be used to check for kill signals
    
    based off https://stackoverflow.com/a/31464349

    TODO Does not work in windows yet
    """
    
    def __init__(self):
        self.kill_now = False
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self,signum, frame):
        self.kill_now = True


def get_path():
    filename = inspect.stack()[1][1]
    path = os.path.abspath(os.path.dirname(filename))
    return path


def write_pid_to_pidfile(pidfile_path):
    """ Write the PID in the named PID file.

        Get the numeric process ID (“PID”) of the current process
        and write it to the named file as a line of text.

        """
    open_flags = (os.O_CREAT | os.O_WRONLY | os.O_TRUNC)
    open_mode = 0o644
    pidfile_fd = os.open(pidfile_path, open_flags, open_mode)
    pidfile = os.fdopen(pidfile_fd, 'w')

    # According to the FHS 2.3 section on PID files in /var/run:
    #
    #   The file must consist of the process identifier in
    #   ASCII-encoded decimal, followed by a newline character. For
    #   example, if crond was process number 25, /var/run/crond.pid
    #   would contain three characters: two, five, and newline.

    pid = os.getpid()
    pidfile.write("%s\n" % pid)
    pidfile.close()
    return pid


def count_ref(obj):
    return sys.getrefcount(obj)


def default_project_init():
    # Get init location
    filename = inspect.stack()[1][1]
    path = os.path.abspath(os.path.dirname(filename))

    # Create init global vars
    from usoro.easypickle import EasyPickle
    container = EasyPickle(filename='globals', always_save=False)
    container.put('path_cwd', os.path.abspath(os.getcwd()))
    container.put('path_app', path)
    
    # Change cwd to init location
    os.chdir(path)

    # Enable logging
    from usoro.logger import Log
    Log.PrepareLogger('log')

    # Create pid file
    pid = write_pid_to_pidfile('process.pid')

    Log.Info('INIT', 'Starting process (%d)' % pid)

    # After init return to call directory
    os.chdir(container.get('path_cwd'))
    return container

def date_time(unixtime, dt_format='%Y-%m-%d %H:%M:%S'):
    """Returns formatted timesamp
    
    Parameters
    ----------
    unixtime : float
        time.time() result
    dt_format : str, optional
        strftime format (the default is '%Y-%m-%d %H:%M:%S', MySQL default)
    
    Returns
    -------
    str
        formatted time string
    
    Tests
    ------
    >>> date_time(1533846359.5567472)
    '2018-08-09 22:25:59'
    >>> date_time(1533846359.5567472, dt_format='%Y-%m-%d')
    '2018-08-09'
    """
    
    d = datetime.datetime.fromtimestamp(unixtime)
    return d.strftime(dt_format)

def import_non_local(name, custom_name=None):
    """Prevent module name clash
    
    Parameters
    ----------
    name : str
        module name
    custom_name : str, optional
        Name for module (the default is None, which simply uses name)
    
    Returns
    -------
    module
        imported module
    """

    import imp, sys

    custom_name = custom_name or name

    f, pathname, desc = imp.find_module(name, sys.path[1:])
    module = imp.load_module(custom_name, f, pathname, desc)
    f.close()

    return module