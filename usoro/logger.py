# System imports
import os
import logging
from logging.handlers import TimedRotatingFileHandler


class Log:

    @staticmethod
    def PrepareLogger(logFileName='log'):
        logFilePath = os.path.join("logs")
        if not os.path.isdir(logFilePath):
            os.makedirs(logFilePath)

        logger = logging.getLogger()
        hdlr = TimedRotatingFileHandler(os.path.join(logFilePath, logFileName), when='D', interval=1, backupCount=0, encoding=None,
                                        delay=False, utc=False)
        formatter = logging.Formatter('%(asctime)s\t%(levelname)s\t%(message)s')
        hdlr.setFormatter(formatter)
        logger.addHandler(hdlr)
        logger.setLevel(logging.NOTSET)

    @staticmethod
    def Debug(tag, msg):
        logging.debug(tag + "\t" + msg)

    @staticmethod
    def Info(tag, msg):
        logging.info(tag + "\t" + msg)

    @staticmethod
    def Warning(tag, msg):
        logging.warning(tag + "\t" + msg)

    @staticmethod
    def Error(tag, msg):
        logging.error(tag + "\t" + msg)

    @staticmethod
    def Critical(tag, msg):
        logging.critical(tag + "\t" + msg)
