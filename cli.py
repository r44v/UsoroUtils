"""
usoro
"""
from usoro.daemon import cli, options_dict, get_pid_file
from app import main_loop
import os

current_dir = os.path.abspath(os.path.dirname(__file__))

options_dict.update({
    "linux_callback": main_loop,
    "default_func": main_loop,
    "executable": os.path.join(current_dir, 'app.py'),
    "pid_file": os.path.join(current_dir, 'process.pid')
})

cli()