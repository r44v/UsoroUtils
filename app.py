"""
usoro
"""
import time
import os
import usoro.utils as uu
from usoro.logger import Log

def main_loop():
    container = uu.default_project_init()

    from multiprocessing import current_process
    Log.Debug(__file__, str(current_process().name == 'MainProcess'))
    
    # Prevent Abort errors
    kill = uu.GracefulKiller()
    
    while 1:
        try:
            time.sleep(1)
            if kill.kill_now:
                os.remove('process.pid')
                Log.Info('KILL', 'Gracefully exited')
                break
        except Exception as e:
            print(e)

if __name__ == '__main__':
    main_loop()